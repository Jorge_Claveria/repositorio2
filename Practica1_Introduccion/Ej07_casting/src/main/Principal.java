package main;

public class Principal {

	public static void main(String[] args) {

		int numeroInt=1234567;
		byte numeroByte;
		numeroByte=(byte)numeroInt;
		System.out.println("Numero Byte= " + numeroByte);
		int numeroShort;
		float numeroFloat=6.5f;//casteo: Transformar de una variable primitiva a otra
		System.out.println("variable float = " + numeroFloat);
		numeroShort=(int) numeroFloat;
		System.out.println("variable short = " + numeroShort);
	}

}
